﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace WorkspaceControl
{
    public class NodeBase
    {
        public enum PlugType
        {
            Plug_Input = 0, 
            Plug_Output
        }
        public enum PlugPositioning
        {
            Plug_LeftTop = 0,
            Plug_MiddleTop,
            Plug_RightTop,
            Plug_LeftMid, 
            Plug_Central,
            Plug_RightMid,
            Plug_LeftBottom,
            Plug_MiddleBottom,
            Plug_RightBottom
        }

        public NodeProperties editorProperties = new NodeProperties();

        private List<NodeBase> child = new List<NodeBase>();
        protected NodeBase parent = null;
        private NodeBase targetNode = null;
        protected WorkspaceControl workspace;
        public NodeBase(NodeTemplate nodeTemplate)
        {
            workspace = nodeTemplate.workspace;
            ApplyPropertiesFromTemplate(nodeTemplate, false);
        }
        public NodeBase() { }
        virtual public void ApplyPropertiesFromTemplate(NodeTemplate nodeTemplate, bool inPlace = true, bool resetPorts = true)
        {
            foreach(string spec in nodeTemplate.specialStringList)
            {
                editorProperties.specialStringList.Add(spec);
            }

            editorProperties.bSpecAll = nodeTemplate.specAll;

            editorProperties.scaleTemp = workspace.scale;
            editorProperties.EDITOR_PROPERTIES.nodeTypeName = nodeTemplate.nodeTypeName;

            editorProperties.descriptionFont = new Font(SystemFonts.DefaultFont.FontFamily, 16 / editorProperties.scaleTemp);
            editorProperties.topInfoFont = new Font(SystemFonts.DefaultFont.FontFamily, 14 / editorProperties.scaleTemp);

            SizeF strDesc = workspace.CreateGraphics().MeasureString(editorProperties.EDITOR_PROPERTIES.nodeDescription, editorProperties.descriptionFont);
            SizeF topInf = workspace.CreateGraphics().MeasureString(editorProperties.EDITOR_PROPERTIES.nodeTypeName, editorProperties.topInfoFont);

            editorProperties.descriptionAdditionalWidth = strDesc.Width - editorProperties.size.Width;
            if (editorProperties.descriptionAdditionalWidth < 0) editorProperties.descriptionAdditionalWidth = 0;

            editorProperties.topInfoAdditionalWidth = topInf.Width - editorProperties.size.Width;
            if (editorProperties.topInfoAdditionalWidth < 0) editorProperties.topInfoAdditionalWidth = 0;

            editorProperties.size = new SizeF(nodeTemplate.size.Width + editorProperties.descriptionAdditionalWidth + editorProperties.topInfoAdditionalWidth, nodeTemplate.size.Height);
            editorProperties.baseSize = nodeTemplate.size;
            editorProperties.inputCreate = nodeTemplate.inputCreate;
            editorProperties.outputCreate = nodeTemplate.outputCreate;
            editorProperties.plugSize = nodeTemplate.plugSize;
            editorProperties.inputPlugPos = nodeTemplate.inputPlugPos;
            editorProperties.outputPlugPos = nodeTemplate.outputPlugPos;
            editorProperties.EDITOR_PROPERTIES.nodeDescription = nodeTemplate.nodeDescription;
            if (!inPlace)
            {
                editorProperties.curPos = workspace.GetMousePos();
                editorProperties.constPos = workspace.GetMousePos();
            }
            editorProperties.EDITOR_PROPERTIES.node_color = nodeTemplate.node_color;
            editorProperties.EDITOR_PROPERTIES.panels_color = nodeTemplate.panels_color;
            editorProperties.EDITOR_PROPERTIES.plugs_color = nodeTemplate.plugs_color;
            editorProperties.bIsSingleUse = nodeTemplate.bIsSingleUse;
            editorProperties.inputPlugPercentPos = nodeTemplate.inputPlugPercentPos;
            editorProperties.outputPlugPercentPos = nodeTemplate.outputPlugPercentPos;
            editorProperties.verticallyOriented = nodeTemplate.verticallyOriented;
            editorProperties.bSingleChild = nodeTemplate.bSingleChild;
            editorProperties.bSpecial = nodeTemplate.bSpecial;
            editorProperties.bEditable = nodeTemplate.bEditable;

            editorProperties.specialMode = nodeTemplate.sSpecialMode;

            Scale();

            if (editorProperties.inputCreate)
            {
                if (editorProperties.inputPlugPos > -1)
                    editorProperties.input = new NodePlug(this, editorProperties.plugSize, editorProperties.inputPlugPos, (int)PlugType.Plug_Input);
                else
                {
                    editorProperties.input = new NodePlug(this, editorProperties.plugSize, editorProperties.inputPlugPercentPos, (int)PlugType.Plug_Input);
                }
            }
            else
            {
                if (editorProperties.input != null)
                {
                    if (parent != null)
                    {
                        parent.RemoveChild(this);
                        parent = null;
                    }
                    editorProperties.input = null;
                }
            }
            if (editorProperties.outputCreate)
            {
                if (editorProperties.outputPlugPos > -1)
                    editorProperties.output = new NodePlug(this, editorProperties.plugSize, editorProperties.outputPlugPos, (int)PlugType.Plug_Output);
                else
                {
                    editorProperties.output = new NodePlug(this, editorProperties.plugSize, editorProperties.outputPlugPercentPos, (int)PlugType.Plug_Output);
                }
            }
            else
            {
                if (editorProperties.output != null)
                {
                    RemoveAllChildren();
                    editorProperties.output = null;
                }
            }

            if (resetPorts)
            {
                editorProperties.ports_in.Clear();
                editorProperties.port_out.Clear();
                foreach (NodePort port_in in nodeTemplate.ports_in)
                {
                    editorProperties.ports_in.Add(new NodePort(workspace, this, editorProperties.plugSize, editorProperties.ports_in.Count, port_in.variableType, port_in.portType, port_in.positionPerc));
                }
                foreach (NodePort port_out in nodeTemplate.ports_out)
                {
                    editorProperties.port_out.Add(new NodePort(workspace, this, editorProperties.plugSize, editorProperties.port_out.Count, port_out.variableType, port_out.portType, port_out.positionPerc));
                }
            }
        }
        virtual public void Draw(Graphics g)
        {
            //if node is pressed, set selected node 
            editorProperties.descriptionFont = new Font(SystemFonts.DefaultFont.FontFamily, 12 / editorProperties.scaleTemp);
            editorProperties.topInfoFont = new Font(SystemFonts.DefaultFont.FontFamily, 14 / editorProperties.scaleTemp);
            SizeF stringSize = g.MeasureString(editorProperties.EDITOR_PROPERTIES.nodeDescription, editorProperties.descriptionFont);
            SizeF topInf = workspace.CreateGraphics().MeasureString(editorProperties.EDITOR_PROPERTIES.nodeTypeName, editorProperties.topInfoFont);
            editorProperties.descriptionAdditionalWidth = stringSize.Width - editorProperties.size.Width;
            if (editorProperties.descriptionAdditionalWidth < 0) editorProperties.descriptionAdditionalWidth = 0;
            editorProperties.topInfoAdditionalWidth = topInf.Width - editorProperties.size.Width;
            if (editorProperties.topInfoAdditionalWidth < 0) editorProperties.topInfoAdditionalWidth = 0;

            var captionRect = new RectangleF(editorProperties.curPos, new SizeF(editorProperties.size.Width + editorProperties.descriptionAdditionalWidth + editorProperties.topInfoAdditionalWidth, editorProperties.size.Height));

            //when group selection in workspace is goint on, select items
            if(workspace.selectionRectangle.Contains(captionRect))
            {
                if(!editorProperties.bIsGroupSelected)
                {
                    GroupSelect(true);
                }
            }
            //if mouse is up, then we assign hover
            if (!workspace.IsMouseDown())
            {
                if (captionRect.Contains(workspace.GetMousePos()))
                {
                    workspace.nodeHover = this;
                }
                //if mouse is no longer over node hover, nullify it
                else if (workspace.nodeHover == this)
                {
                    workspace.nodeHover = null;
                }
                //if selected node is this node and mouse is up, then update its position and stop dragging it
                if (workspace.activeNode == this)
                {
                    UpdateConstPos();
                    editorProperties.bIsDragging = false;
                    //also if has active plug, show context menu; snap plug MUST be null
                    if (editorProperties.activePlug != null && workspace.snapPlugB == null && workspace.snapPlugA != null)
                    {
                        workspace.ShowContextMenu(workspace.GetMousePos());
                    }
                }
            }
            else
            {
                if(workspace.nodeHover == null)
                {
                    if (!workspace.bIsGroupSelecting)
                        workspace.database.DeselectAll();
                }
                //if mouse is down, then check if there was node hover, if there was, select it
                if (workspace.nodeHover == this)
                {
                    if (!workspace.bIsGroupSelecting && !workspace.bIsGroupDeselecting)
                    {
                        if (!editorProperties.bIsGroupSelected)
                        {
                            Select(true);
                            if (workspace.GetMouseStartPos() != workspace.GetMousePos())
                            {
                                editorProperties.bIsDragging = true;
                            }
                        }
                    }
                    else if(workspace.bIsGroupSelecting)
                    {
                        if(workspace.activeNode != null)
                        {
                            workspace.activeNode.GroupSelect(true);
                            workspace.activeNode.Select(false);
                        }
                        GroupSelect(true);
                    }
                    else if (workspace.bIsGroupDeselecting)
                    {
                        GroupSelect(false);
                    }
                }
                //if mouse hover is not this and there is node selected and it's not this, then find apropriate plug
                else if (workspace.activeNode != null && workspace.activeNode != this)
                {
                    //if(workspace.activeNode)
                }
            }
            //if selected node is this node, then drag it
            if (workspace.activeNode == this && editorProperties.bIsDragging && editorProperties.activePlug == null && editorProperties.activePort == null)
            {
                //update mouse pos to be able to drag rectangle when mouse is down
                PointF mousePos = workspace.GetMousePos();
                PointF startMousePos = workspace.GetMouseStartPos();
                PointF mpDiff = new PointF(startMousePos.X - editorProperties.constPos.X, startMousePos.Y - editorProperties.constPos.Y);
                mousePos = new PointF(mousePos.X - mpDiff.X, mousePos.Y - mpDiff.Y);

                editorProperties.curPos = mousePos;
            }
            //below is pretty much the same, but in case that we group selected nodes
            else if(editorProperties.bIsGroupSelected && workspace.IsMouseDown())
            {
                PointF mousePos = workspace.GetMousePos();
                PointF startMousePos = workspace.GetMouseStartPos();
                PointF mpDiff = new PointF(startMousePos.X - editorProperties.constPos.X, startMousePos.Y - editorProperties.constPos.Y);
                mousePos = new PointF(mousePos.X - mpDiff.X, mousePos.Y - mpDiff.Y);

                editorProperties.curPos = mousePos;
            }


            if (editorProperties.bIsGroupSelected || workspace.activeNode == this)
                editorProperties.node_currentColor = editorProperties.node_selected;
            else
            {
                editorProperties.node_currentColor = editorProperties.EDITOR_PROPERTIES.node_color;
            }

            if (!workspace.IsMouseDown())
                UpdateConstPos();

            //rectangles scale calculation
            if (editorProperties.scaleTemp != workspace.scale)
                ReScale();
            //main rectangle
            RectangleF mainRect = DrawMainRectangle(g);
            //top info rectangle
            DrawTopInfoRectangle(g);
            //description rectangle
            DrawDescriptionRectangle(g, mainRect);
            //draw plugs first, because if mouse is over plugs, then break attempt to drag ndoe window
            DrawInput(g);
            DrawOutput(g);
            //draw ports
            foreach(NodePort port_in in editorProperties.ports_in)
            {
                port_in.Draw(g);
            }
            foreach(NodePort port_out in editorProperties.port_out)
            {
                port_out.Draw(g);
            }
            //evey node stores its single connection with parent, this connection is drawn here
            if(GetParent() != null)
            {
                if (editorProperties.verticallyOriented)
                {            
                    Point pt1 = GetParent().GetOutputPlugPoint();
                    Point pt2 = new Point(GetParent().GetOutputPlugPoint().X, GetParent().GetOutputPlugPoint().Y + (int)(80 / workspace.scale));
                    Point pt3 = new Point(GetInputPlugPoint().X, GetInputPlugPoint().Y - (int)(80 / workspace.scale));
                    Point pt4 = GetInputPlugPoint();

                    g.DrawBezier(new Pen(GetParent().editorProperties.EDITOR_PROPERTIES.plugs_color, 3), pt1, pt2, pt3, pt4);
                }
                else
                    g.DrawLines(new Pen(GetParent().editorProperties.EDITOR_PROPERTIES.plugs_color, 3), FuryMath.GetCurveAlgorithmPoints(GetParent().GetOutputPlugPoint(), GetInputPlugPoint()));
            }
            //sort children based on x axis position
            NodeBase temp = null;
            for(int i = 0; i< child.Count; i++)
            {
                for(int j = 0; j < child.Count - i; j++)
                {
                    if (child.Count >= j + 2)
                    {
                        if (child.ElementAt<NodeBase>(j + 1) != null)
                        {
                            if (child.ElementAt<NodeBase>(j).editorProperties.curPos.X > child.ElementAt<NodeBase>(j + 1).editorProperties.curPos.X)
                            {
                                temp = child.ElementAt<NodeBase>(j);
                                child[j] = child.ElementAt<NodeBase>(j + 1);
                                child[j + 1] = temp;
                            }
                        }
                    }
                }
            }
        }

        virtual protected void DrawInput(Graphics g)
        {
            if (editorProperties.input != null) editorProperties.input.Draw(g);
        }
        virtual protected void DrawOutput(Graphics g)
        {
            if (editorProperties.output != null) editorProperties.output.Draw(g);
        }

        protected RectangleF DrawMainRectangle(Graphics g)
        {
            var mainRect = new RectangleF(editorProperties.curPos, new SizeF(editorProperties.size.Width + editorProperties.descriptionAdditionalWidth + editorProperties.topInfoAdditionalWidth, editorProperties.size.Height));
            g.FillRectangle(new SolidBrush(editorProperties.node_currentColor), mainRect);
            g.DrawRectangle(new Pen(Color.Black, 2), Rectangle.Round(mainRect));
            return mainRect;
        }
        protected void DrawTopInfoRectangle(Graphics g)
        {
            //type label
            {
                SizeF strSize = g.MeasureString(editorProperties.EDITOR_PROPERTIES.nodeTypeName, editorProperties.topInfoFont);
                PointF rectPos = new PointF(editorProperties.curPos.X, editorProperties.curPos.Y + (editorProperties.plugSize.Height / editorProperties.scaleTemp));

                var topInfoRect = new RectangleF(rectPos, new SizeF(editorProperties.size.Width + editorProperties.descriptionAdditionalWidth + editorProperties.topInfoAdditionalWidth, editorProperties.size.Height * 0.25f));
                g.FillRectangle(new SolidBrush(editorProperties.EDITOR_PROPERTIES.panels_color), topInfoRect);
                g.DrawRectangle(new Pen(Color.Black, 2), Rectangle.Round(topInfoRect));
           
                float str_x_loc = topInfoRect.Location.X + (topInfoRect.Width / 2f) - (strSize.Width / 2f);


                g.DrawString(editorProperties.EDITOR_PROPERTIES.nodeTypeName, editorProperties.topInfoFont, new SolidBrush(WorkspaceControl.furyGold), new PointF(str_x_loc, topInfoRect.Location.Y));
            }
        }
        virtual protected void DrawDescriptionRectangle(Graphics g, RectangleF mainRect)
        {
            //desc label
            {
                // if (editorProperties.scaleTemp == 1)
                {
                    SizeF strSize = g.MeasureString(editorProperties.EDITOR_PROPERTIES.nodeDescription, editorProperties.descriptionFont);
                    PointF rectPos = new PointF(editorProperties.curPos.X, editorProperties.curPos.Y + (int)mainRect.Height - (editorProperties.plugSize.Height / editorProperties.scaleTemp) - (editorProperties.size.Height * 0.48f));

                    var descRect = new RectangleF(rectPos, new SizeF(editorProperties.size.Width + editorProperties.descriptionAdditionalWidth + editorProperties.topInfoAdditionalWidth, editorProperties.size.Height * 0.48f));
                    g.FillRectangle(new SolidBrush(editorProperties.EDITOR_PROPERTIES.panels_color), descRect);
                    g.DrawRectangle(new Pen(Color.Black, 2), Rectangle.Round(descRect));

                    float str_x_loc = descRect.Location.X + (descRect.Width / 2f) - (strSize.Width / 2f);

                    g.DrawString(editorProperties.EDITOR_PROPERTIES.nodeDescription, editorProperties.descriptionFont, new SolidBrush(WorkspaceControl.furyGold), new PointF(str_x_loc, rectPos.Y + (descRect.Height / 2) - (editorProperties.descriptionFont.Height / 2)));
                }
            }
        }
        protected void Scale()
        {
            float scaleTemp_saved = editorProperties.scaleTemp;
            editorProperties.scaleTemp = workspace.scale;

            editorProperties.size = new SizeF(editorProperties.baseSize.Width / editorProperties.scaleTemp, editorProperties.baseSize.Height / editorProperties.scaleTemp);
        }
        public void ReScale()
        {
            float scaleTemp_saved = editorProperties.scaleTemp;
            editorProperties.scaleTemp = workspace.scale;

            SizeF newSize = new SizeF((editorProperties.baseSize.Width + editorProperties.descriptionAdditionalWidth + editorProperties.topInfoAdditionalWidth) / editorProperties.scaleTemp, editorProperties.baseSize.Height / editorProperties.scaleTemp);

            editorProperties.size = newSize;

            PointF basePos = new PointF(editorProperties.curPos.X * scaleTemp_saved, editorProperties.curPos.Y * scaleTemp_saved);
            editorProperties.curPos = basePos;

            editorProperties.curPos = new PointF(editorProperties.curPos.X / editorProperties.scaleTemp, editorProperties.curPos.Y / editorProperties.scaleTemp);
        }
        virtual public PointF CalculatePlugPosition(int posEnum, Size plugSize, PointF percentageAmount)
        {
            if (posEnum > -1)
            {
                float x_axis = 0;
                float y_axis = 0;
                //TOP; y axis never changes; LeftTop is 0, 0 always
                if (posEnum == (int)PlugPositioning.Plug_MiddleTop)
                {
                    x_axis = (editorProperties.size.Width / 2) - editorProperties.plugSize.Width / 2;
                }
                else if (posEnum == (int)PlugPositioning.Plug_RightTop)
                {
                    x_axis = editorProperties.size.Width - editorProperties.plugSize.Width;
                }
                //MIDDLE; y axis is the same for all of them
                else if (posEnum == (int)PlugPositioning.Plug_LeftMid)
                {
                    y_axis = (editorProperties.size.Height / 2) - editorProperties.plugSize.Height / 2;
                }
                else if (posEnum == (int)PlugPositioning.Plug_Central)
                {
                    y_axis = (editorProperties.size.Height / 2) - editorProperties.plugSize.Height / 2;
                    x_axis = (editorProperties.size.Width / 2) - editorProperties.plugSize.Width / 2;
                }
                else if (posEnum == (int)PlugPositioning.Plug_RightMid)
                {
                    y_axis = (editorProperties.size.Height / 2) - editorProperties.plugSize.Height / 2;
                    x_axis = editorProperties.size.Width - editorProperties.plugSize.Width;
                }
                //BOTTOM; y axis stays the same
                else if (posEnum == (int)PlugPositioning.Plug_LeftBottom)
                {
                    y_axis = editorProperties.size.Height - editorProperties.plugSize.Height;
                }
                else if (posEnum == (int)PlugPositioning.Plug_MiddleBottom)
                {
                    y_axis = editorProperties.size.Height - editorProperties.plugSize.Height;
                    x_axis = (editorProperties.size.Width / 2) - editorProperties.plugSize.Width / 2;
                }
                else if (posEnum == (int)PlugPositioning.Plug_RightBottom)
                {
                    y_axis = editorProperties.size.Height - editorProperties.plugSize.Height;
                    x_axis = editorProperties.size.Width - editorProperties.plugSize.Width;
                }

                return new PointF(x_axis + editorProperties.curPos.X, y_axis + editorProperties.curPos.Y);
            }
            else
            {
                //counting percentage of the node size to put plug
                //X axis
                SizeF nodeSize = new SizeF(editorProperties.size.Width + editorProperties.descriptionAdditionalWidth, editorProperties.size.Height);
                float x_axis_pos = 0f;
                {
                    float percentage = (percentageAmount.X <= 100f) ? percentageAmount.X : 100f;
                    percentage /= 100f;
                    x_axis_pos = nodeSize.Width * percentage;
                    x_axis_pos -= plugSize.Width / 2;
                }
                //Y axis
                float y_axis_pos = 0f;
                {
                    float percentage = (percentageAmount.Y <= 100f) ? percentageAmount.Y : 100f;
                    percentage /= 100f;
                    y_axis_pos = nodeSize.Height * percentage;
                    y_axis_pos -= plugSize.Height / 2;
                }
                return new PointF(x_axis_pos + editorProperties.curPos.X, y_axis_pos + editorProperties.curPos.Y);
            }
        }

        virtual public PointF CalculatePortPosition(int portIndex, Size portSize, int portType, PointF posPerc)
        {
            if (posPerc == new PointF(-999f, -999f))
            {
                float x_axis = editorProperties.curPos.X;
                if (portType == (int)PlugType.Plug_Output)
                    x_axis += editorProperties.size.Width - portSize.Width;

                //Y axis calculations
                float y_axis = editorProperties.curPos.Y;

                if (portType == (int)PlugType.Plug_Input)
                {
                    if (editorProperties.input != null)
                        y_axis += GetInputPlugPointLocal().Y;
                }
                else if (portType == (int)PlugType.Plug_Output)
                {
                    if (editorProperties.input != null)
                        y_axis += GetOutputPlugPointLocal().Y;
                }

                y_axis += 10f;
                y_axis += (portSize.Width + 5f) * portIndex;

                return new PointF(x_axis, y_axis);
            }
            else
            {
                //counting percentage of the node size to put plug
                //X axis
                SizeF nodeSize = editorProperties.size;
                float x_axis_pos = 0f;
                {
                    float percentage = (posPerc.X <= 100f) ? posPerc.X : 100f;
                    percentage /= 100f;
                    x_axis_pos = nodeSize.Width * percentage;
                    x_axis_pos -= editorProperties.plugSize.Width / 2;
                }
                //Y axis
                float y_axis_pos = 0f;
                {
                    float percentage = (posPerc.Y <= 100f) ? posPerc.Y : 100f;
                    percentage /= 100f;
                    y_axis_pos = nodeSize.Height * percentage;
                    y_axis_pos -= editorProperties.plugSize.Height / 2;
                }
                return new PointF(x_axis_pos + editorProperties.curPos.X, y_axis_pos + editorProperties.curPos.Y);
            }
        }

        public void Refresh(Graphics g, Pen p)
        {
            if(parent != null)
            {
                Point output = parent.GetOutputPlugPoint();
                Point input = GetInputPlugPoint();
                g.DrawLines(p, FuryMath.GetCurveAlgorithmPoints(output, input));
            }
        }

        public WorkspaceControl GetWorkspace()
        {
            return workspace;
        }
        public void SetActivePlug(NodePlug plug)
        {
            editorProperties.activePlug = plug;
        }
        public void SetActivePort(NodePort port)
        {
            editorProperties.activePort = port;
        }
        public Point GetActivePlugPoint()
        {
            if (editorProperties.activePlug != null)
                return editorProperties.activePlug.GetPoint();

            return new Point(0, 0);
        }
        public NodePlug GetActivePlug()
        {
            return editorProperties.activePlug;
        }
        public NodePort GetActivePort()
        {
            return editorProperties.activePort;
        }
        public void SetTargetNode(NodeBase target)
        {
            targetNode = target;
        }
        public void AddChild(NodeBase newChild)
        {
            child.Add(newChild);

            NodeBase savedParent = newChild.parent;
            newChild.parent = this;
            if (savedParent != null)
                savedParent.RemoveChild(newChild);
        }
        public void RemoveChild(NodeBase childToRemove)
        {
            child.Remove(childToRemove);
        }
        public void RemoveAllChildren()
        {
            foreach(NodeBase kid in child)
            {
                kid.parent = null;
            }
            child.Clear();
        }
        public void ApplyTargetNode()
        {
            if (editorProperties.activePlug == editorProperties.output)
            {
                AddChild(targetNode);
            }
            else if (editorProperties.activePlug == editorProperties.input)
            {
                targetNode.AddChild(this);
            }
        }
        virtual public Point GetOutputPlugPoint()
        {
            return editorProperties.output.GetPoint();
        }
        virtual public Point GetInputPlugPoint()
        {
            return editorProperties.input.GetPoint();
        }
 
        public NodeBase GetParent()
        {
            return parent;
        }
        public void SetParent(NodeBase newParent)
        {
            parent = newParent;
        }
        public NodeBase GetTargetNode()
        {
            return targetNode;
        }
        public void UpdateConstPos()
        {
            editorProperties.constPos = editorProperties.curPos;
        }
        public void Select(bool sel)
        {
            if (sel)
            {
                workspace.database.DeselectAll();

                if (workspace.activeNode != null && workspace.activeNode != this)
                    workspace.activeNode.Select(false);

                editorProperties.node_currentColor = editorProperties.node_selected;
                workspace.activeNode = this;
            }
            else
            {
                editorProperties.node_currentColor = editorProperties.EDITOR_PROPERTIES.node_color;
                workspace.activeNode = null;
            }
        }
        public void GroupSelect(bool sel)
        {
            if(sel)
            {
                workspace.database.SelectNode(this);
                editorProperties.bIsGroupSelected = true;
            }
            else
            {
                workspace.database.DeselectNode(this);
                editorProperties.bIsGroupSelected = false;
            }
        }
        public void GroupSelectPure(bool sel)
        {
            editorProperties.bIsGroupSelected = false;
        }
        public void Move(PointF pos)
        {
            editorProperties.curPos = pos;
        }
        public PointF GetInputPlugPointLocal()
        {
            if(editorProperties.input != null)
            {
                return editorProperties.input.connectionPointLocal;
            }
            return new Point(0, 0);
        }
        public PointF GetOutputPlugPointLocal()
        {
            if (editorProperties.output != null)
            {
                return editorProperties.output.connectionPointLocal;
            }
            return new Point(0, 0);
        }
        public PointF GetCurrentPos()
        {
            return editorProperties.curPos;
        }
        public void Delete()
        {
            if (parent != null)
            {
                parent.RemoveChild(this);
                parent = null;
            }
            foreach(NodeBase kid in child)
            {
                kid.parent = null;
            }
        }
        public List<NodeBase> GetChildren()
        {
            return child;
        }
        public void SetColors(Color _base, Color _panels, Color _plugs)
        {
            editorProperties.EDITOR_PROPERTIES.node_color = _base;
            editorProperties.EDITOR_PROPERTIES.panels_color = _panels;
            editorProperties.EDITOR_PROPERTIES.plugs_color = _plugs;
        }
        public bool HasChildren()
        {
            return child.Count > 0;
        }
        public NodePort GetFirstPortIn()
        {
            foreach(NodePort portIn in editorProperties.ports_in)
            {
                return portIn;
            }
            return null;
        }
    }
}
