﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace WorkspaceControl
{
    public class NodeInfoContext
    {
        private SizeF size;
        private readonly WorkspaceControl workspace;
        public NodeInfoContext(WorkspaceControl wrk)
        {
            workspace = wrk;
            size = new SizeF(50, 50);
        }
        public void Draw(Graphics g, NodeBase node)
        {
            if (node == null)
                return;
            NodeProperties prop = node.editorProperties;
            SolidBrush textBrush = new SolidBrush(Color.White);

            //calculate node type
            Font typeFont = new Font(SystemFonts.DefaultFont.FontFamily.Name, 13f);
            SizeF nodeTypeLabelSize = g.MeasureString(prop.EDITOR_PROPERTIES.nodeTypeName, typeFont);
            //calculate node description
            Font descFont = new Font(SystemFonts.DefaultFont.FontFamily.Name, 10f);
            SizeF nodeDescLabelSize = g.MeasureString(prop.EDITOR_PROPERTIES.nodeDescription, descFont);
            //calculate rectangle size
            float max_x = 0f;
            float max_y = 0f;
            if (nodeTypeLabelSize.Width < nodeDescLabelSize.Width) max_x = nodeDescLabelSize.Width;
            else max_x = nodeTypeLabelSize.Width;
            max_x += 20f;
            if (prop.EDITOR_PROPERTIES.nodeDescription != null && prop.EDITOR_PROPERTIES.nodeDescription.Length != 0)
                max_y += nodeDescLabelSize.Height + 10f;
            max_y += nodeTypeLabelSize.Height + 20f;
            size = new SizeF(max_x, max_y);

            var rect = new RectangleF(workspace.GetMousePos(), size);

            g.FillRectangle(new SolidBrush(WorkspaceControl.furyGrayXtreme), rect);
            g.DrawRectangle(new Pen(new SolidBrush(Color.Black), 3f), Rectangle.Round(rect));

            //draw node type
            PointF typeLoc = new PointF(rect.Location.X + 10f, rect.Location.Y + 10f);
            g.DrawString(prop.EDITOR_PROPERTIES.nodeTypeName, typeFont, textBrush, typeLoc);
            //draw node description
            g.DrawString(prop.EDITOR_PROPERTIES.nodeDescription, descFont, textBrush, new PointF(typeLoc.X, typeLoc.Y + typeFont.Height + 10f));
        }
    }
}
