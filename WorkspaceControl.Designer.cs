﻿namespace WorkspaceControl
{
    partial class WorkspaceControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // WorkspaceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.DoubleBuffered = true;
            this.Name = "WorkspaceControl";
            this.Size = new System.Drawing.Size(462, 379);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.WorkspaceControl_Paint);
            this.KeyDown += WorkspaceControl_KeyDown;
            this.KeyUp += WorkspaceControl_KeyUp;
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.WorkspaceControl_MouseClick);
            this.MouseMove += WorkspaceControl_MouseMove;
            this.MouseDown += WorkspaceControl_MouseDown;
            this.MouseUp += WorkspaceControl_MouseUp;
            this.MouseWheel += WorkspaceControl_MouseWheel;
            this.ResumeLayout(false);

        }

        #endregion

    }
}

