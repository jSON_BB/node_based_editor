﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WorkspaceControl
{
    public class NodeTemplate
    {
        public readonly string nodeTypeName;
        public string nodeDescription;
        public readonly WorkspaceControl workspace;
        public readonly Size size;
        public readonly bool inputCreate;
        public readonly bool outputCreate;
        public readonly Size plugSize;
        public readonly int inputPlugPos = -1;
        public readonly int outputPlugPos = -1;
        public readonly PointF inputPlugPercentPos;
        public readonly PointF outputPlugPercentPos;
        public readonly Color node_color = WorkspaceControl.furyGrayDark;
        public readonly Color panels_color = WorkspaceControl.furyGray;
        public readonly Color plugs_color = WorkspaceControl.furyDarkRed;
        public readonly bool bIsSingleUse;
        public readonly bool verticallyOriented = false;
        public readonly bool bSingleChild = false;
        public readonly bool bSpecial = false;
        public readonly bool bEditable = true;
        public readonly string sSpecialMode = "";
        public List<string> specialStringList = new List<string>();
        public bool specAll = false;
        //Advanced
        public readonly List<NodePort> ports_in = new List<NodePort>();
        public readonly List<NodePort> ports_out = new List<NodePort>();
        //~Advanced
        public NodeTemplate(string nodeName, WorkspaceControl workspaceParent, Size _size, bool _input, bool _output, Size _plugSize, int _inputPlugPos, int _outputPlugPos, Color color_base, Color color_panels, Color color_plugs, bool _single, bool vertical = false, bool singleChild = false, bool _special = false, bool _editable = true, string specialMode = "")
        {
            nodeTypeName = nodeName;
            workspace = workspaceParent;
            size = _size;
            inputCreate = _input;
            outputCreate = _output;
            plugSize = _plugSize;
            inputPlugPos = _inputPlugPos;
            outputPlugPos = _outputPlugPos;
            node_color = color_base;
            panels_color = color_panels;
            plugs_color = color_plugs;
            bIsSingleUse = _single;
            verticallyOriented = vertical;
            bSingleChild = singleChild;
            bSpecial = _special;
            bEditable = _editable;
            sSpecialMode = specialMode;
        }
        public NodeTemplate(string nodeName, WorkspaceControl workspaceParent, Size _size, bool _input, bool _output, Size _plugSize, PointF _inputPlugPercentPos, PointF _outputPlugPercentPos, Color color_base, Color color_panels, Color color_plugs, bool _single, bool vertical = false, bool singleChild = false, bool _special = false, bool _editable = true, string specialMode = "")
        {
            nodeTypeName = nodeName;
            workspace = workspaceParent;
            size = _size;
            inputCreate = _input;
            outputCreate = _output;
            plugSize = _plugSize;
            inputPlugPos = -1;
            outputPlugPos = -1;
            inputPlugPercentPos = _inputPlugPercentPos;
            outputPlugPercentPos = _outputPlugPercentPos;
            node_color = color_base;
            panels_color = color_panels;
            plugs_color = color_plugs;
            bIsSingleUse = _single;
            verticallyOriented = vertical;
            bSingleChild = singleChild;
            bSpecial = _special;
            bEditable = _editable;
            sSpecialMode = specialMode;
        }
        /// <summary>
        /// ADVANCED NODE TEMPLATE
        /// </summary>
        /// <param name="nodeName"></param>
        /// <param name="workspaceParent"></param>
        /// <param name="_size"></param>
        /// <param name="color_base"></param>
        /// <param name="color_top"></param>
        /// <param name="_singleUse"></param>
        /// <param name="_vertical"></param>
        /// <param name="singleChild"></param>
        /// <param name="_special"></param>
        /// <param name="_editable"></param>
        public NodeTemplate(string nodeName, WorkspaceControl workspaceParent, Size _size, Color color_base, Color color_top, bool _singleUse = false, bool _vertical = false, bool singleChild = true, bool _special = false, bool _editable = false)
        {
            nodeTypeName = nodeName;
            workspace = workspaceParent;
            size = _size;
            node_color = color_base;
            panels_color = color_top;
            bIsSingleUse = _singleUse;
            verticallyOriented = _vertical;
            bSingleChild = singleChild;
            bSpecial = _special;
            bEditable = _editable;
        }

        public bool HasPlugType(int plugType)
        {
            if(plugType == (int)NodeBase.PlugType.Plug_Input)
                return inputCreate;
            else if (plugType == (int)NodeBase.PlugType.Plug_Output)
                return outputCreate;

            return false;
        }
        public bool HasOppositePlugType(int plugType)
        {
            if (plugType == (int)NodeBase.PlugType.Plug_Input)
                return outputCreate;
            else if (plugType == (int)NodeBase.PlugType.Plug_Output)
                return inputCreate;

            return false;
        }
        public void AddInputPort(Size _size, int vartype)
        {
            ports_in.Add(new NodePort(workspace, _size, vartype, (int)NodeBase.PlugType.Plug_Input));
        }
        public void AddInputPort(Size _size, int vartype, PointF percPos)
        {
            ports_in.Add(new NodePort(workspace, _size, vartype, (int)NodeBase.PlugType.Plug_Input, percPos));
        }
        public void AddOutputPort(Size _size, int vartype)
        {
            ports_out.Add(new NodePort(workspace, _size, vartype, (int)NodeBase.PlugType.Plug_Output));
        }
        public void AddOutputPort(Size _size, int vartype, PointF percPos)
        {
            ports_out.Add(new NodePort(workspace, _size, vartype, (int)NodeBase.PlugType.Plug_Output, percPos));
        }
    }
}
