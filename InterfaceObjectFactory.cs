﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WorkspaceControl
{
    public class InterfaceObjectFactory
    {
        private readonly WorkspaceControl workspace;
        private readonly WorkspaceDatabase database = null;
        public InterfaceObjectFactory(WorkspaceDatabase db, WorkspaceControl workspaceParent)
        {
            workspace = workspaceParent;
            database = db;
        }
        public InterfaceObject CreateInterfaceObject(InterfaceObjectTemplate objTemplate, InterfaceObject parentObject)
        {
            if (objTemplate == null)
                return null;

            InterfaceObject newObj = new InterfaceObject(objTemplate, parentObject);

            if (newObj != null)
                database.AddInterfaceObject(newObj);
            return newObj;
        }
        public void RegisterInterfaceObject(InterfaceObjectTemplate templ)
        {
            database.RegisterInterfaceObject(templ);
        }
    }
}
