﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace WorkspaceControl
{
    public class NodePort
    {
        public enum PlugVariableColor
        {
            Bool = 0,
            Float,
            Int,
            String,
            Vector3, 
            EntityId, 
            EntityPointer
        }
        public Color[] variableColors = { Color.DarkRed, Color.Green, Color.Blue, Color.Violet, Color.Yellow, Color.Beige, Color.Chartreuse };

        private readonly WorkspaceControl workspace = null;
        private Timer timer = new Timer();
        private readonly NodeBase parentNode;
        private Size portSize;
        private Point connectionPoint;
        public int variableType = -1;
        public readonly int portType = -1;
        private NodePort parentPort = null;

        private readonly int index = -1;

        public PointF positionPerc = new PointF(-999f, -999f);
        public NodePort(WorkspaceControl wrk, NodeBase _parentNode, Size _size, int idx, int vartype, int _type, PointF percPos)
        {
            workspace = wrk;
            parentNode = _parentNode;
            portSize = _size;
            index = idx;
            variableType = vartype;
            portType = _type;
            positionPerc = percPos;

            var rect = new RectangleF(parentNode.CalculatePortPosition(index, portSize, portType, positionPerc), portSize);
            connectionPoint = new Point((int)rect.Location.X + (int)(rect.Width / 2), (int)rect.Location.Y + (int)(rect.Height / 2));
        }
        public NodePort(WorkspaceControl wrk, Size _size, int vartype, int _type)
        {
            workspace = wrk;
            portSize = _size;
            variableType = vartype;
            portType = _type;
        }
        public NodePort(WorkspaceControl wrk, Size _size, int vartype, int _type, PointF percPos)
        {
            workspace = wrk;
            portSize = _size;
            variableType = vartype;
            portType = _type;
            positionPerc = percPos;
        }
        public void Draw(Graphics g)
        {
            //caption rectangle to check if it's hovered
            var rect = new RectangleF(parentNode.CalculatePortPosition(index, portSize, portType, positionPerc), portSize);
            connectionPoint = new Point((int)rect.Location.X + (int)(rect.Width / 2), (int)rect.Location.Y + (int)(rect.Height / 2));

            //if mouse is up, then we check hover
            if(!workspace.IsMouseDown())
            {
                if(rect.Contains(workspace.GetMousePos()))
                {
                    //assing it only if parent node is selected
                    parentNode.editorProperties.portHover = this;
                    g.FillRectangle(new SolidBrush(WorkspaceControl.furyGold), rect);
                }
                //if mouse is no longer over this port, nullify it
                else if(parentNode.editorProperties.portHover == this)
                {
                    parentNode.editorProperties.portHover = null;
                }
                //if mouse is up then deactivate port
                parentNode.SetActivePort(null);
                //apply connection if mouse is up and this is snap port
                if(workspace.snapPortB == this)
                {
                    if (IsOutput())
                        workspace.activeNode.GetActivePort().parentPort = this;
                    else
                        parentPort = workspace.snapPortA;

                    workspace.snapPortA = null;
                    workspace.snapPortB = null;
                    if (workspace.activeNode != null)
                        workspace.activeNode.SetActivePort(null);
                }
            }
            //if mouse is dont and parent node is the selected one, and port is hovered then activate it
            else if(workspace.activeNode == parentNode && parentNode.editorProperties.portHover == this)
            {
                parentNode.SetActivePort(this);
                workspace.snapPortA = this;
            }
            //if port is outside node
            else if(parentNode.editorProperties.portHover == this)
            {
                workspace.activeNode = parentNode;
                parentNode.SetActivePort(this);
                workspace.snapPortA = this;
            }
            //default color
            if(parentNode.editorProperties.portHover != this)
            {
                g.FillRectangle(new SolidBrush(variableColors[variableType]), rect);
            }
            //if this is active port then highlight it
            if(parentNode.GetActivePort() == this)
            {
                g.FillRectangle(new SolidBrush(WorkspaceControl.furyGold), rect);
            }
            //if mouse is down and over this port, then check if it's linkable
            else if(workspace.activeNode != null && workspace.activeNode.GetActivePort() != null)
            {
                if(workspace.IsMouseDown() && rect.Contains(workspace.GetMousePos()))
                {
                    if(CanLinkInto())
                    {
                        g.FillRectangle(new SolidBrush(WorkspaceControl.furyGold), rect);
                        workspace.snapPortB = this;
                    }
                }
                else
                {
                    //if mouse is off the previously snapped port, reset it
                    if(workspace.snapPortB == this)
                    {
                        workspace.snapPortB = null;
                    }
                }
            }

            g.DrawRectangle(new Pen(Color.Black, 2), Rectangle.Round(rect));
            //drawing connection with port parent
            if(parentPort != null)
            {
                g.DrawLines(new Pen(parentPort.variableColors[variableType], 3), FuryMath.GetCurveAlgorithmPoints(parentPort.GetPoint(), GetPoint()));
            }
        }
        public bool CanLinkInto()
        {
            bool bOK = false;
            //firstly check if active node exists
            if (workspace.activeNode != null)
            {
                //if currently active node's active plug is opposite type; 
                //if active node is not myself
                //if I am not parent of the active node

                bOK = ((workspace.activeNode.GetActivePort().GetPortType() != GetPortType()) && (workspace.activeNode != parentNode) && (workspace.activeNode.GetParent() != parentNode) && (parentNode.GetParent() != parentNode));
            }


            return bOK;
        }
        public Point GetPoint()
        {
            return connectionPoint;
        }
        public NodeBase GetNode()
        {
            return parentNode;
        }
        public bool IsOutput()
        {
            return (portType == (int)NodeBase.PlugType.Plug_Output);
        }
        public bool IsInput()
        {
            return (portType == (int)NodeBase.PlugType.Plug_Input);
        }
        public int GetPortType()
        {
            return portType;
        }
    }
}
