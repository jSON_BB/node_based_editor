﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace WorkspaceControl
{
    public class NodeProperties
    {
        //PUBLIC EDITABLE PROPERTIES
        public class EditorProperties
        {
            public string nodeTypeName;
            public string nodeDescription;
            public Color node_color = WorkspaceControl.furyGrayDark;
            public Color panels_color = WorkspaceControl.furyGray;
            public Color plugs_color = WorkspaceControl.furyDarkRed;
        }
        public EditorProperties EDITOR_PROPERTIES = new EditorProperties();
        //
        //PRIVATE NON-EDITABLE PROPERTIES
        public readonly Color node_selected = WorkspaceControl.furyGoldOxygened;
        public Color node_currentColor = WorkspaceControl.furyGrayDark;
        public bool bIsDragging = false;
        public SizeF size;
        public SizeF baseSize;
        public bool inputCreate;
        public bool outputCreate;
        public Size plugSize;
        public int inputPlugPos;
        public int outputPlugPos;
        public PointF constPos = new PointF(0, 0);
        public PointF curPos = new PointF(0, 0);
        public NodePlug output;
        public NodePlug input;
        public NodePlug activePlug = null;
        public NodePort activePort = null;
        public NodePlug plugHover = null;
        public NodePort portHover = null;
        public bool bIsGroupSelected = false;
        public float scaleTemp = 0;
        public bool bIsSingleUse = false;
        public PointF inputPlugPercentPos;
        public PointF outputPlugPercentPos;
        public bool verticallyOriented = false;
        public bool bSingleChild = false;
        public bool bSpecial = false;
        public string filePath;
        public bool bEditable = true;
        public List<NodePort> ports_in = new List<NodePort>();
        public List<NodePort> port_out = new List<NodePort>();
        public Font descriptionFont = null;
        public float descriptionAdditionalWidth = 0f;
        public Font topInfoFont = null;
        public float topInfoAdditionalWidth = 0f;
        public string specialMode = "";
        public List<string> specialStringList = new List<string>();
        public bool bSpecAll = false;
        //
    }
}
