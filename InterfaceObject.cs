﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Text;

namespace WorkspaceControl
{
    public class InterfaceObject
    {
        public enum EMouseControlEvent
        {
            eMC_None = -1,
            eMC_Enter,
            eMC_Leave, 
            eMC_Press, 
            eMC_Release, 
            eMC_Hovering,
            eMC_InputTextChanged,
            eMC_InputTextEmpty, 
            eMC_InputTextNotEmpty,
            eMC_DropDownListValueChanged,
            eMC_SearchListValueSelected
        }

        public InterfaceObjectProperties editorProperties = new InterfaceObjectProperties();
        InterfaceObjectProperties.HierarchyObject selectedHierarchyObj = null;

        protected WorkspaceControl workspace;

        public static float CORRECT_PERC = 0.68f;

        private bool bMouseEnter = false;
        private bool bMouseLeave = false;
        private bool bPressed = false;
        private bool bReleased = false;

        private bool bSwitchSelected = false;

        public InterfaceObject() { }
        public InterfaceObject(InterfaceObjectTemplate templ, InterfaceObject parentObject)
        {
            workspace = templ.workspace;
            editorProperties.parentObject = parentObject;
            ApplyPropertiesFromTemplate(templ);
        }
        virtual public void ApplyPropertiesFromTemplate(InterfaceObjectTemplate nodeTemplate)
        {
            editorProperties.scaleTemp = workspace.scale;
            editorProperties.EDITOR_PROPERTIES.objectTypeName = nodeTemplate.objectTypeName;

            editorProperties.size = nodeTemplate.size;
            editorProperties.baseSize = nodeTemplate.size;

            Scale();
        }
        private void DrawEditModeLogic(Graphics g)
        {
            if (!WorkspaceControl.bEditing)
                return;

            bSwitchSelected = false;
            editorProperties.bVisible = true;

            DrawResizeRectangles(g);

            var captionRect = new RectangleF(editorProperties.globalPos, editorProperties.size);

            if(workspace.selectionRectangle.Contains(captionRect))
            {
                if (!editorProperties.bIsGroupSelected)
                    GroupSelect(true);
            }
            if(!workspace.IsMouseDown())
            {
                if (captionRect.Contains(workspace.GetMousePos()))
                    workspace.objectHover = this;
                else if (workspace.objectHover == this)
                    workspace.objectHover = null;
                if(workspace.activeObject == this)
                {
                    UpdateConstPos();
                    editorProperties.bIsDragging = false;
                }
            }
            else
            {
                if(workspace.objectHover == null)
                {
                    if (!workspace.bIsGroupSelecting)
                        workspace.database.DeselectAll();
                }
                if(workspace.objectHover == this)
                {
                    if (!workspace.bIsGroupDeleting && !workspace.bIsGroupDeselecting)
                    {
                        if (!editorProperties.bIsGroupSelected)
                        {
                            Select(true);
                            if (workspace.GetMouseStartPos() != workspace.GetMousePos())
                                editorProperties.bIsDragging = true;
                        }
                    }
                    else if (workspace.bIsGroupSelecting)
                    {
                        if (workspace.activeObject != null)
                        {
                            workspace.activeObject.GroupSelect(true);
                            workspace.activeObject.Select(false);
                        }
                        GroupSelect(true);
                    }
                    else if (workspace.bIsGroupDeselecting)
                        GroupSelect(false);
                }
            }
            if ((workspace.activeObject == this && editorProperties.bIsDragging) || (editorProperties.bIsGroupSelected && workspace.IsMouseDown()))
                DragObject();

            UpdateGlobalPos();

            if (editorProperties.scaleTemp != workspace.scale)
                ReScale();

            //Draw visual rectangle
            g.DrawRectangle(new Pen(Color.Black, 2), Rectangle.Round(captionRect));

            if (workspace.activeObject == this || editorProperties.bIsGroupSelected)
                g.FillRectangle(new SolidBrush(Color.LightGray), captionRect);
        }
        private void DrawResizeRectangles(Graphics g)
        {
            string name = editorProperties.EDITOR_PROPERTIES.objectTypeName;
            if (name == "Label" || name == "Button")
                return;

            if (name == "Panel" && editorProperties.EDITOR_PROPERTIES.backgroundImage != null)
                return;

            if (workspace.activeObject == this || workspace.resizedObject == this || workspace.resizedObject_Sealed == this)
            {
                //Draw resize rectangles
                RectangleF resize_x_rect = new RectangleF();
                RectangleF resize_y_rect = new RectangleF();
                SizeF resize_rect_size = new SizeF(10f, 10f);
                //X
                {
                    PointF resize_x_rect_pos = new PointF(editorProperties.globalPos.X + editorProperties.size.Width, editorProperties.globalPos.Y + (editorProperties.size.Height / 2) - (resize_rect_size.Height / 2));
                    resize_x_rect = new RectangleF(resize_x_rect_pos, resize_rect_size);
                    g.DrawRectangle(new Pen(Color.Black, 1), Rectangle.Round(resize_x_rect));
                }
                //Y
                if (name != "Input_Text")
                {
                    PointF resize_y_rect_pos = new PointF(editorProperties.globalPos.X + (editorProperties.size.Width / 2) - (resize_rect_size.Width / 2), editorProperties.globalPos.Y + editorProperties.size.Height);
                    resize_y_rect = new RectangleF(resize_y_rect_pos, resize_rect_size);
                    g.DrawRectangle(new Pen(Color.Black, 1), Rectangle.Round(resize_y_rect));
                }

                if (workspace.activeObject == this || workspace.resizedObject == this)
                {
                    if (resize_x_rect.Contains(workspace.GetMousePos()))
                    {
                        g.FillRectangle(new SolidBrush(Color.Green), resize_x_rect);
                        workspace.resizedObject = this;

                        if (workspace.IsMouseDown())
                        {
                            workspace.resizedObject_Sealed = this;
                            editorProperties.bResizingY = false;
                            editorProperties.bResizingX = true;
                        }
                        else
                        {
                            workspace.resizedObject_Sealed = null;
                            editorProperties.bResizingY = false;
                            editorProperties.bResizingX = false;
                        }
                    }
                    else if (resize_y_rect.Contains(workspace.GetMousePos()))
                    {
                        g.FillRectangle(new SolidBrush(Color.Green), resize_y_rect);
                        workspace.resizedObject = this;

                        if (workspace.IsMouseDown())
                        {
                            workspace.resizedObject_Sealed = this;
                            editorProperties.bResizingY = true;
                            editorProperties.bResizingX = false;
                        }
                        else
                        {
                            workspace.resizedObject_Sealed = null;
                            editorProperties.bResizingY = false;
                            editorProperties.bResizingX = false;
                        }
                    }
                    else
                        workspace.resizedObject = null;

                }
                else if (workspace.resizedObject_Sealed == this)
                {
                    if (workspace.IsMouseDown())
                    {
                        if (!editorProperties.bIsResizing)
                        {
                            if(editorProperties.bResizingX)
                                editorProperties.x_resize_start = workspace.GetMousePos().X;
                            else if(editorProperties.bResizingY)
                                editorProperties.y_resize_start = workspace.GetMousePos().Y;

                            editorProperties.bIsResizing = true;
                        }

                        float fDiff = 0f;
                        if (editorProperties.bResizingX)
                        {
                            fDiff = workspace.GetMousePos().X - editorProperties.x_resize_start;
                            editorProperties.x_resize_start = workspace.GetMousePos().X;
                            editorProperties.baseSize.Width += fDiff;
                            ReScale();
                        }
                        else if (editorProperties.bResizingY)
                        {
                            fDiff = workspace.GetMousePos().Y - editorProperties.y_resize_start;
                            editorProperties.y_resize_start = workspace.GetMousePos().Y;
                            editorProperties.baseSize.Height += fDiff;
                            ReScale();
                        }
                    }
                    else
                        editorProperties.bIsResizing = false;
                }
            }

            if(!workspace.IsMouseDown())
            {
                editorProperties.bIsResizing = false;
                workspace.resizedObject_Sealed = null;
            }
        }
        private void DrawTestModeLogic(Graphics g)
        {
            if ((editorProperties.bHide || (editorProperties.parentObject != null && editorProperties.parentObject.IsHidden())) && WorkspaceControl.bEditing)
                return;

            if(!WorkspaceControl.bEditing)
            {
                if (!editorProperties.bVisible || (editorProperties.parentObject != null && !editorProperties.parentObject.editorProperties.bVisible))
                    return;
            }

            string name = editorProperties.EDITOR_PROPERTIES.objectTypeName;

            EMouseControlEvent eventInfo = DrawHitRectangle(g, editorProperties.globalPos, editorProperties.size, editorProperties.EDITOR_PROPERTIES.objectTypeName);

            if (name == "Panel")
                DrawPanel(g);
            else if (name == "MessageBox")
                DrawPanel(g);
            else if (name == "Label")
                DrawLabel(g, editorProperties.EDITOR_PROPERTIES.labelText, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, true, true, editorProperties.EDITOR_PROPERTIES.labelBaseColor);
            else if (name == "Button")
                DrawButton(g, eventInfo);
            else if (name == "Input_Text")
                DrawInputText(g);
            else if (name == "DropDownList")
                DrawDropDownList(g, eventInfo);
            else if (name == "SearchList")
                DrawSearchList(g);
            else if (name == "SwitchButton")
                DrawSwitchButton(g, eventInfo);
            else if (name == "DropTarget")
                DrawDropTarget(g, eventInfo);
        }
        //full objects
        private void DrawPanel(Graphics g)
        {
            //Panel has only one image and should be visible in both edit and test mode
            DrawImage(g, editorProperties.EDITOR_PROPERTIES.backgroundImage, editorProperties.globalPos, editorProperties.size);
            DrawImage(g, editorProperties.EDITOR_PROPERTIES.additionalIconImage, editorProperties.globalPos, editorProperties.size);
        }
        private void DrawButton(Graphics g, EMouseControlEvent eventInfo)
        {
            //Button has all types of images in order; Base image is shown only if event is none
            //Base image is the only one visible in both modes
            if (eventInfo == EMouseControlEvent.eMC_None)
                DrawImage(g, editorProperties.EDITOR_PROPERTIES.backgroundImage, editorProperties.globalPos, editorProperties.size);
            //Hover and pressed images are drawn only when in test mode and only under certain circumstances; They also utilize hitRect
            else if (eventInfo == EMouseControlEvent.eMC_Hovering)
                DrawImage(g, editorProperties.EDITOR_PROPERTIES.hoverImage, editorProperties.globalPos, editorProperties.size);
            else if (eventInfo == EMouseControlEvent.eMC_Press)
                DrawImage(g, editorProperties.EDITOR_PROPERTIES.pressedImage, editorProperties.globalPos, editorProperties.size);
            //Additional icon also applies here, but it is on top of regular images and is shown in both modes
            DrawImage(g, editorProperties.EDITOR_PROPERTIES.additionalIconImage, editorProperties.globalPos, editorProperties.size);
            //Labels can also be part of button drawing and is done in exactly same way as images; same story as with the images
            if (eventInfo == EMouseControlEvent.eMC_None)
                DrawLabel(g, editorProperties.EDITOR_PROPERTIES.labelText, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, true, true, editorProperties.EDITOR_PROPERTIES.labelBaseColor);
            else if (eventInfo == EMouseControlEvent.eMC_Hovering)
                DrawLabel(g, editorProperties.EDITOR_PROPERTIES.labelText, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, true, true, editorProperties.EDITOR_PROPERTIES.labelHoverColor);
            else if (eventInfo == EMouseControlEvent.eMC_Press)
                DrawLabel(g, editorProperties.EDITOR_PROPERTIES.labelText, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, true, true, editorProperties.EDITOR_PROPERTIES.labelPressedColor);
        }
        private void DrawSwitchButton(Graphics g, EMouseControlEvent eventInfo)
        {
            //Switch has all types of images in order; It is almost exactly same as button, with exception for locking system and group; same thing as with regular buttons
            if (eventInfo == EMouseControlEvent.eMC_None)
                DrawImage(g, editorProperties.EDITOR_PROPERTIES.backgroundImage, editorProperties.globalPos, editorProperties.size);
            //Mouse events work only if is not selected
            if (!bSwitchSelected)
            {
                if (eventInfo == EMouseControlEvent.eMC_Hovering)
                    DrawImage(g, editorProperties.EDITOR_PROPERTIES.hoverImage, editorProperties.globalPos, editorProperties.size);
                else if (eventInfo == EMouseControlEvent.eMC_Press)
                    DrawImage(g, editorProperties.EDITOR_PROPERTIES.pressedImage, editorProperties.globalPos, editorProperties.size);
            }
            else
                DrawImage(g, editorProperties.EDITOR_PROPERTIES.pressedImage, editorProperties.globalPos, editorProperties.size);
            //Additional icon also applies here, but it is on top of regular images and is shown in both modes
            DrawImage(g, editorProperties.EDITOR_PROPERTIES.additionalIconImage, editorProperties.globalPos, editorProperties.size);
            //Labels can also be part of button drawing and is done in exactly same way as images
            if (eventInfo == EMouseControlEvent.eMC_None)
                DrawLabel(g, editorProperties.EDITOR_PROPERTIES.labelText, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, true, true, editorProperties.EDITOR_PROPERTIES.labelBaseColor);
            if (!bSwitchSelected)
            {
                if (eventInfo == EMouseControlEvent.eMC_Hovering)
                    DrawLabel(g, editorProperties.EDITOR_PROPERTIES.labelText, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, true, true, editorProperties.EDITOR_PROPERTIES.labelHoverColor);
                else if (eventInfo == EMouseControlEvent.eMC_Press)
                    DrawLabel(g, editorProperties.EDITOR_PROPERTIES.labelText, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, true, true, editorProperties.EDITOR_PROPERTIES.labelPressedColor);
            }
            else
                DrawLabel(g, editorProperties.EDITOR_PROPERTIES.labelText, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, true, true, editorProperties.EDITOR_PROPERTIES.labelPressedColor);
        }
        private void DrawInputText(Graphics g)
        {
            //Input text need background for label and then label on the top on both modes
            DrawLabelBackground(g);
            //Also its text will be different in test mode and edit mode
            string txt = editorProperties.EDITOR_PROPERTIES.labelText;
            if (!WorkspaceControl.bEditing)
                txt = editorProperties.inputTextCurrentString;
            DrawLabel(g, txt, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, false, true, editorProperties.EDITOR_PROPERTIES.labelBaseColor);
        }
        private void DrawDropDownList(Graphics g, EMouseControlEvent eventInfo)
        {
            //Drop down list needs main item as label button and items of the same type
            //Selected item already has hitbox by default, so we put only label there
            if (eventInfo == EMouseControlEvent.eMC_None)
                DrawLabel(g, editorProperties.dropDownListMainSelector, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, false, true, editorProperties.EDITOR_PROPERTIES.labelBaseColor);
            //interactive label is to be drawn only in test mode
            if (!WorkspaceControl.bEditing)
            {
                if (eventInfo == EMouseControlEvent.eMC_Hovering)
                    DrawLabel(g, editorProperties.dropDownListMainSelector, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, false, true, editorProperties.EDITOR_PROPERTIES.labelHoverColor);
                else if (eventInfo == EMouseControlEvent.eMC_Press)
                    DrawLabel(g, editorProperties.dropDownListMainSelector, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, false, true, editorProperties.EDITOR_PROPERTIES.labelPressedColor);
            }
            //Items dont have hit boxes yet, so we create those, but we need to measure string first to get idea of the hit size for item
            if (editorProperties.EDITOR_PROPERTIES.labelFont == null || editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC < 1)
                return;

            //do not draw items in test mode and when is not expanded
            if (!WorkspaceControl.bEditing && !editorProperties.bDropDownExpanded)
                return;

            Font mainFont = new Font(editorProperties.EDITOR_PROPERTIES.labelFont, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC / editorProperties.scaleTemp, FontStyle.Regular);
            float mainFontHeight = mainFont.Height;
            float labSize = editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC / editorProperties.scaleTemp;
            labSize -= labSize * 0.2f;
            Font itemFont = new Font(editorProperties.EDITOR_PROPERTIES.labelFont, labSize, FontStyle.Regular);
            int idx = 1;
            foreach(string itemStr in editorProperties.dropDownItemList)
            {
                float y_add = mainFontHeight * idx;
                idx++;
                PointF itemPos = new PointF(editorProperties.globalPos.X, editorProperties.globalPos.Y + y_add);
                SizeF itemSize = g.MeasureString(itemStr, itemFont);
                //hit box
                EMouseControlEvent hitEvent = DrawHitRectangle(g, itemPos, itemSize, itemStr, false);
                //actual string
                if (hitEvent == EMouseControlEvent.eMC_None)
                    DrawLabel(g, itemStr, labSize, editorProperties.EDITOR_PROPERTIES.labelFont, false, false, editorProperties.EDITOR_PROPERTIES.labelBaseColor, true, itemPos);
                //interactive label is to be drawn only in test mode
                if (!WorkspaceControl.bEditing)
                {
                    if (hitEvent == EMouseControlEvent.eMC_Hovering)
                        DrawLabel(g, itemStr, labSize, editorProperties.EDITOR_PROPERTIES.labelFont, false, false, editorProperties.EDITOR_PROPERTIES.labelHoverColor, true, itemPos);
                    else if (hitEvent == EMouseControlEvent.eMC_Press)
                    {
                        DrawLabel(g, itemStr, labSize, editorProperties.EDITOR_PROPERTIES.labelFont, false, false, editorProperties.EDITOR_PROPERTIES.labelPressedColor, true, itemPos);
                        DropDownListClose();
                        if (editorProperties.dropDownListMainSelector != itemStr)
                        {
                            editorProperties.dropDownListMainSelector = itemStr;
                            workspace.OnInterfaceObjectTestModeEvent(this, EMouseControlEvent.eMC_DropDownListValueChanged);
                        }
                    }
                }
                //
            }
        }
        private void DrawSearchList(Graphics g)
        {
            //search list is similar to drop down list, but it is expanded fully all the time; it expands other lists though
            //as it already has hit box by default, it will store its size; we need to give it some border though

            if (editorProperties.EDITOR_PROPERTIES.labelFont == null || editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC < 1)
                return;

            //drawing items is just as simple as in drop down list with additional expand icon
            //in edit mode entire list is visible
            //items are pretty much like buttons; so first we draw base label
            int idx = 0;
            foreach (InterfaceObjectProperties.HierarchyObject hierarchyObj in editorProperties.hierarchyObjects)
            {
                float xPos = editorProperties.globalPos.X;
                SizeF strSize = MeasureString(g, editorProperties.EDITOR_PROPERTIES.labelFont, (int)(editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC), hierarchyObj.name);
                strSize.Width += 30;
                float yPos = (strSize.Height * idx) + editorProperties.globalPos.Y;
                EMouseControlEvent eventInfo = DrawHitRectangle(g, new PointF(xPos, yPos), strSize, hierarchyObj.name, false);
                var itemBorderRect = new RectangleF(new PointF(xPos, yPos), strSize);
                g.DrawRectangle(new Pen(Color.White, 2), Rectangle.Round(itemBorderRect));
                if (eventInfo == EMouseControlEvent.eMC_None)
                    DrawLabel(g, hierarchyObj.name, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, false, false, editorProperties.EDITOR_PROPERTIES.labelBaseColor, true, new PointF(xPos, yPos));

                if (!WorkspaceControl.bEditing)
                {
                    if (eventInfo == EMouseControlEvent.eMC_Hovering)
                        DrawLabel(g, hierarchyObj.name, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, false, false, editorProperties.EDITOR_PROPERTIES.labelHoverColor, true, new PointF(xPos, yPos));
                    else if (eventInfo == EMouseControlEvent.eMC_Press)
                    {
                        DrawLabel(g, hierarchyObj.name, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, false, false, editorProperties.EDITOR_PROPERTIES.labelPressedColor, true, new PointF(xPos, yPos));
                        SearchListExpandHierarchy(hierarchyObj);
                    }
                }

                if (hierarchyObj.children.Count > 0)
                {
                    if (editorProperties.EDITOR_PROPERTIES.additionalIconImage != null)
                    {
                        DrawImage(g, editorProperties.EDITOR_PROPERTIES.additionalIconImage, new PointF(xPos + strSize.Width - 15, yPos + (strSize.Height / 2) - 7), new SizeF(15, 15));
                    }
                }

                if(WorkspaceControl.bEditing || hierarchyObj.bTestModeExpanded)
                    DrawSearchListHierarchyItem(g, hierarchyObj, xPos + strSize.Width, yPos);

                idx++;
            }

        }
        private void DrawSearchListHierarchyItem(Graphics g, InterfaceObjectProperties.HierarchyObject obj, float parentXPosEnd, float parentYPos)
        {
            int idx = 0;
            foreach (InterfaceObjectProperties.HierarchyObject child in obj.children)
            {
                float xPos = parentXPosEnd;
                SizeF strSize = MeasureString(g, editorProperties.EDITOR_PROPERTIES.labelFont, (int)(editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC), child.name);
                strSize.Width += 30;
                float yPos = (strSize.Height * idx + 1) + parentYPos;
                EMouseControlEvent eventInfo = DrawHitRectangle(g, new PointF(xPos, yPos), strSize, editorProperties.EDITOR_PROPERTIES.objectTypeName);
                var itemBorderRect = new RectangleF(new PointF(xPos, yPos), strSize);
                g.DrawRectangle(new Pen(Color.White, 2), Rectangle.Round(itemBorderRect));
                if (eventInfo == EMouseControlEvent.eMC_None)
                    DrawLabel(g, child.name, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, false, false, editorProperties.EDITOR_PROPERTIES.labelBaseColor, true, new PointF(xPos, yPos));

                if (!WorkspaceControl.bEditing)
                {
                    if (eventInfo == EMouseControlEvent.eMC_Hovering)
                        DrawLabel(g, child.name, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, false, false, editorProperties.EDITOR_PROPERTIES.labelHoverColor, true, new PointF(xPos, yPos));
                    else if (eventInfo == EMouseControlEvent.eMC_Press)
                    {
                        DrawLabel(g, child.name, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, editorProperties.EDITOR_PROPERTIES.labelFont, false, false, editorProperties.EDITOR_PROPERTIES.labelPressedColor, true, new PointF(xPos, yPos));
                        SearchListExpandHierarchy(child);
                    }
                }

                if (child.children.Count > 0)
                {
                    if (editorProperties.EDITOR_PROPERTIES.additionalIconImage != null)
                    {
                        DrawImage(g, editorProperties.EDITOR_PROPERTIES.additionalIconImage, new PointF(xPos + strSize.Width - 15, yPos + (strSize.Height / 2) - 7), new SizeF(15, 15));
                    }
                }
                if (WorkspaceControl.bEditing || child.bTestModeExpanded)
                    DrawSearchListHierarchyItem(g, child, xPos + strSize.Width, yPos);
                idx++;
            }
        }
        public void SearchListCloseHierarchy(InterfaceObjectProperties.HierarchyObject obj, bool noParented = false)
        {
            if(obj != null)
            {
                if (!noParented)
                {
                    foreach (InterfaceObjectProperties.HierarchyObject childObj in obj.children)
                    {
                        if (childObj.bTestModeExpanded)
                        {
                            SearchListCloseHierarchy(childObj);
                            childObj.bTestModeExpanded = false;
                        }
                    }
                }
                else
                {
                    foreach(InterfaceObjectProperties.HierarchyObject childObj in obj.parentProperties.hierarchyObjects)
                    {
                        if(childObj.bTestModeExpanded)
                        {
                            SearchListCloseHierarchy(childObj);
                            childObj.bTestModeExpanded = false;
                        }
                    }
                }
            }
        }
        private void SearchListExpandHierarchy(InterfaceObjectProperties.HierarchyObject obj)
        {
            //firstly we have to check if parent of selected object has any expanded hierarchies and close it entirely
            if (obj.parent != null)
                SearchListCloseHierarchy(obj.parent);
            //if there is no parent we need to close its own hierarchy
            else
                SearchListCloseHierarchy(obj, true);
            //then open up required hierarchy
            obj.bTestModeExpanded = true;

            if(obj.children.Count == 0)
            {
                if (selectedHierarchyObj != obj)
                {
                    SearchListCloseHierarchy(obj, true);
                    selectedHierarchyObj = obj;
                    workspace.OnInterfaceObjectTestModeEvent(this, EMouseControlEvent.eMC_SearchListValueSelected);
                }
            }

        }
        private void DrawDropTarget(Graphics g, EMouseControlEvent eventInfo)
        {
            //Base image is shown when no event from mouse
            DrawImage(g, editorProperties.EDITOR_PROPERTIES.backgroundImage, editorProperties.globalPos, editorProperties.size);
            //In drop targets, hover image is available image and pressed image is unavailable
        }
        //~full objects
        //modules
        private EMouseControlEvent DrawHitRectangle(Graphics g, PointF pos, SizeF size, string name, bool sendEvent = true)
        {
            if (WorkspaceControl.bEditing)
                return EMouseControlEvent.eMC_None;

            //Hit rectangle is done entirely for corresponding with mouse
            var hitRect = new RectangleF(pos, size);
            EMouseControlEvent returnValue = EMouseControlEvent.eMC_None;
            if (hitRect.Contains(workspace.GetMousePos()))
            {
                if (!workspace.IsMouseDown())
                {
                    if (sendEvent)
                    {
                        TestMode_Released(name);
                        TestMode_MouseEnter(name);
                        TestMode_Hovered(name);
                    }
                    returnValue = EMouseControlEvent.eMC_Hovering;
                }
                else
                {
                    if(sendEvent)
                        TestMode_Pressed(name);
                    returnValue = EMouseControlEvent.eMC_Press;
                }
            }
            else if(sendEvent)
                TestMode_MouseLeave(name);

            return returnValue;
        }
        private void DrawLabel(Graphics g, string text, float size, FontFamily fontFamily, bool centerX, bool centerY, Color color, bool bCustomPos = false, PointF customPos = new PointF())
        {
            if (text.Length < 1 || size < 1 || fontFamily == null || color == null)
                return;

            Font fnt = new Font(fontFamily, size / editorProperties.scaleTemp, FontStyle.Regular);
            SizeF strSize = g.MeasureString(text, fnt);
            PointF globalPos = editorProperties.globalPos;
            SizeF curSize = editorProperties.size;
            PointF textPos = editorProperties.globalPos;
            if (!bCustomPos)
            {
                if (centerX)
                    textPos.X = globalPos.X + (curSize.Width / 2f) - (strSize.Width / 2f);
                if (centerY)
                    textPos.Y = globalPos.Y + (curSize.Height / 2f) - (strSize.Height / 2f);
            }
            else
                textPos = customPos;

            g.DrawString(text, fnt, new SolidBrush(color), textPos);
        }
        private void DrawImage(Graphics g, Image img, PointF pos, SizeF size)
        {
            if (img == null)
                return;

            SizeF imgScaledSize = new SizeF(img.Width / editorProperties.scaleTemp, img.Height / editorProperties.scaleTemp);
            g.DrawImage(img, pos.X + (editorProperties.size.Width / 2) - (imgScaledSize.Width / 2), pos.Y + (editorProperties.size.Height / 2) - (imgScaledSize.Height / 2), imgScaledSize.Width, imgScaledSize.Height);
        }
        private void DrawLabelBackground(Graphics g)
        {
            var rect = new RectangleF(editorProperties.globalPos, editorProperties.size);
            g.FillRectangle(new SolidBrush(Color.White), rect);

            if (WorkspaceControl.bEditing)
                return;

            if(rect.Contains(workspace.GetMousePos()))
            {
                if(workspace.IsMouseDown())
                {
                    editorProperties.bEditingInputText = true;
                    workspace.TestModeEditingInputText = this;
                }
            }
            else
            {
                if(workspace.IsMouseDown())
                {
                    editorProperties.bEditingInputText = false;
                    if (workspace.TestModeEditingInputText == this)
                        workspace.TestModeEditingInputText = null;
                }
            }
        }
        //~modules
        private SizeF MeasureString(Graphics g, FontFamily fontFamily, int fontSize, string text)
        {
            if (fontFamily == null || fontSize < 1 || text.Length < 1)
                return new SizeF(0f, 0f);

            Font font = new Font(fontFamily, fontSize / editorProperties.scaleTemp, FontStyle.Regular);
            return g.MeasureString(text, font);
        }
        public void Draw(Graphics g)
        {
            DrawEditModeLogic(g);
            DrawTestModeLogic(g);
        }
        private void DropDownListExpand()
        {

        }
        private void DropDownListClose()
        {
            editorProperties.bDropDownExpanded = false;
        }
        private void DragObject()
        {
            PointF mousePos = workspace.GetMousePos();
            PointF startMousePos = workspace.GetMouseStartPos();
            PointF mpDiff = new PointF(startMousePos.X - editorProperties.constPos.X, startMousePos.Y - editorProperties.constPos.Y);
            mousePos = new PointF(mousePos.X - mpDiff.X, mousePos.Y - mpDiff.Y);

            MoveObject(mousePos);
        }
        public void MoveObject_Local(PointF localDestinationPoint)
        {
            if (editorProperties.parentObject != null)
            {
                PointF parentGlobalPos = editorProperties.parentObject.GetGlobalPos();
                PointF globalDestinationPoint = new PointF(parentGlobalPos.X + localDestinationPoint.X, parentGlobalPos.Y + localDestinationPoint.Y);
                if (IsWithinParentRange_x(globalDestinationPoint.X) && IsWithinParentRange_y(globalDestinationPoint.Y))
                {
                    editorProperties.posOnParent = localDestinationPoint;
                    editorProperties.lastCorrect_PosOnParent = editorProperties.posOnParent;
                }
                else
                    editorProperties.posOnParent = editorProperties.lastCorrect_PosOnParent;
            }
            else
                editorProperties.posOnParent = localDestinationPoint;
        }
        public void MoveObject(PointF globalDestinationPoint)
        {
            if(editorProperties.parentObject != null)
            {
                if(IsWithinParentRange_x(globalDestinationPoint.X) && IsWithinParentRange_y(globalDestinationPoint.Y))
                {
                    PointF parentGlobalPos = editorProperties.parentObject.GetGlobalPos();
                    editorProperties.posOnParent = new PointF(globalDestinationPoint.X - parentGlobalPos.X, globalDestinationPoint.Y - parentGlobalPos.Y);
                    editorProperties.lastCorrect_PosOnParent = editorProperties.posOnParent;
                }
                else
                    editorProperties.posOnParent = editorProperties.lastCorrect_PosOnParent;
            }
            else
                editorProperties.posOnParent = globalDestinationPoint;
        }
        public PointF GetClosestCorrectPointForChild(PointF proposedPoint, SizeF proposedSize)
        {
            float proposed_x = proposedPoint.X;
            float proposed_y = proposedPoint.Y;
            float proposed_w = proposedSize.Width;
            float proposed_h = proposedSize.Height;

            //calculating X
            if (proposed_x + proposed_w > editorProperties.globalPos.X + editorProperties.size.Width)
                proposed_x -= (proposed_x + proposed_w) - (editorProperties.globalPos.X + editorProperties.size.Width);
            //calculating Y
            if (proposed_y + proposed_h > editorProperties.globalPos.Y + editorProperties.size.Height)
                proposed_y -= (proposed_y + proposed_h) - (editorProperties.globalPos.Y + editorProperties.size.Height);

            return new PointF(proposed_x, proposed_y);
        }
        public bool IsAbleToSizeDown(SizeF newSize)
        {
            float new_bound_x = editorProperties.globalPos.X + newSize.Width;
            float new_bound_y = editorProperties.globalPos.Y + newSize.Height;

            foreach(InterfaceObject child in editorProperties.children)
            {
                float child_bound_x = child.editorProperties.globalPos.X + child.editorProperties.size.Width;
                float child_bound_y = child.editorProperties.globalPos.Y + child.editorProperties.size.Height;

                if (new_bound_x <= child_bound_x || new_bound_y <= child_bound_y)
                    return false;
            }

            return true;
        }
        public bool IsAbleToSizeUp(SizeF newSize)
        {
            float new_bound_x = editorProperties.globalPos.X + newSize.Width;
            float new_bound_y = editorProperties.globalPos.Y + newSize.Height;

            if(editorProperties.parentObject != null)
            {
                float parent_bound_x = editorProperties.parentObject.editorProperties.globalPos.X + editorProperties.parentObject.editorProperties.size.Width;
                float parent_bound_y = editorProperties.parentObject.editorProperties.globalPos.Y + editorProperties.parentObject.editorProperties.size.Height;

                if (new_bound_x >= parent_bound_x || new_bound_y >= parent_bound_y)
                    return false;
            }

            return true;
        }
        public void UpdateGlobalPos()
        {
            if (editorProperties.parentObject != null)
            {
                PointF parentHierarchyPos = editorProperties.parentObject.GetHierarchyPos();
                editorProperties.globalPos = new PointF(parentHierarchyPos.X + editorProperties.posOnParent.X, parentHierarchyPos.Y + editorProperties.posOnParent.Y);
            }
            else
                editorProperties.globalPos = editorProperties.posOnParent;

            if (!workspace.IsMouseDown())
                UpdateConstPos();
        }
        public bool PositionChanged()
        {
            if (editorProperties.lastPosOnParent != editorProperties.posOnParent)
            {
                editorProperties.lastPosOnParent = editorProperties.posOnParent;
                return true;
            }
            else
                return false;
        }
        public bool IsWithinParentRange_x(float pt_x)
        {
            if (editorProperties.parentObject == null)
                return true;

            return ((pt_x + editorProperties.size.Width < editorProperties.parentObject.editorProperties.globalPos.X + editorProperties.parentObject.editorProperties.size.Width) && (pt_x > editorProperties.parentObject.editorProperties.globalPos.X));
        }
        public bool IsWithinParentRange_y(float pt_y)
        {
            if (editorProperties.parentObject == null)
                return true;

            return ((pt_y + editorProperties.size.Height < editorProperties.parentObject.editorProperties.globalPos.Y + editorProperties.parentObject.editorProperties.size.Height) && (pt_y > editorProperties.parentObject.editorProperties.globalPos.Y));
        }
        private void DrawAdditionalIcon(Graphics g)
        {
            if (editorProperties.EDITOR_PROPERTIES.additionalIconImage == null)
                return;

            g.DrawImage(editorProperties.EDITOR_PROPERTIES.additionalIconImage, editorProperties.globalPos);
        }
        private void TestMode_MouseEnter(string name)
        {
            bMouseLeave = false;
            if(!bMouseEnter)
            {
                bMouseEnter = true;
                OnMouseEnter();
            }  
        }
        private void OnMouseEnter()
        {
            workspace.OnInterfaceObjectTestModeEvent(this, EMouseControlEvent.eMC_Enter);
        }
        private void TestMode_MouseLeave(string name)
        {
            bMouseEnter = false;
            if(!bMouseLeave)
            {
                bMouseLeave = true;
                OnMouseLeave();
            }
        }
        private void OnMouseLeave()
        {
            workspace.OnInterfaceObjectTestModeEvent(this, EMouseControlEvent.eMC_Leave);
        }
        private void TestMode_Hovered(string name)
        {
            if(bMouseEnter)
            {
                OnHover();
            }
        }
        private void OnHover()
        {
            workspace.OnInterfaceObjectTestModeEvent(this, EMouseControlEvent.eMC_Hovering);
        }
        private void TestMode_Pressed(string name)
        {
            if (name != "Button" && name != "SwitchButton" && name != "DropDownList" && editorProperties.EDITOR_PROPERTIES.objectTypeName != "SearchList")
                return;

            if (name == "SearchList")
                return;

            bReleased = false;
            if(!bPressed)
            {
                bPressed = true;
                if (name == "Button" || name == "SwitchButton")
                    OnPressed();
            }
        }
        private void OnPressed()
        {
            workspace.OnInterfaceObjectTestModeEvent(this, EMouseControlEvent.eMC_Press);
        }
        private void TestMode_Released(string name)
        {
            if (name != "Button" && name != "SwitchButton" && name != "DropDownList" && editorProperties.EDITOR_PROPERTIES.objectTypeName != "SearchList")
                return;

            if (name == "SearchList")
                return;

            bPressed = false;
            if(!bReleased && bMouseEnter)
            {
                bReleased = true;
                if (name == "DropDownList")
                {
                    editorProperties.bDropDownExpanded = !editorProperties.bDropDownExpanded;

                    if (editorProperties.bDropDownExpanded)
                    {
                        DropDownListExpand();
                    }
                    else
                    {
                        DropDownListClose();
                    }
                }
                else if (editorProperties.EDITOR_PROPERTIES.objectTypeName == "SearchList")
                {
                    Console.WriteLine(name);
                }
                else if (name == "Button" || name == "SwitchButton") 
                    OnReleased();
            }
        }
        private void OnReleased()
        {
            workspace.OnInterfaceObjectTestModeEvent(this, EMouseControlEvent.eMC_Release);
        }
        private void Scale()
        {
            float scaleTemp_saved = editorProperties.scaleTemp;
            editorProperties.scaleTemp = workspace.scale;

            editorProperties.size = new SizeF(editorProperties.baseSize.Width / editorProperties.scaleTemp, editorProperties.baseSize.Height / editorProperties.scaleTemp);
        }
        public void ReScale()
        {
            float scaleTemp_saved = editorProperties.scaleTemp;
            editorProperties.scaleTemp = workspace.scale;

            SizeF newSize = new SizeF(editorProperties.baseSize.Width / editorProperties.scaleTemp, editorProperties.baseSize.Height / editorProperties.scaleTemp);
            editorProperties.size = newSize;

            PointF basePos = new PointF(editorProperties.posOnParent.X * scaleTemp_saved, editorProperties.posOnParent.Y * scaleTemp_saved);
            editorProperties.posOnParent = basePos;

            editorProperties.posOnParent = new PointF(editorProperties.posOnParent.X / editorProperties.scaleTemp, editorProperties.posOnParent.Y / editorProperties.scaleTemp);
            int i = 0;
        }
        public WorkspaceControl GetWorkspace()
        {
            return workspace;
        }
        public void UpdateConstPos()
        {
            editorProperties.constPos = editorProperties.globalPos;
        }
        public void Select(bool sel)
        {
            if(sel)
            {
                workspace.database.DeselectAll();

                if (workspace.activeObject != null && workspace.activeObject != this)
                    workspace.activeObject.Select(false);

                workspace.activeObject = this;
            }
            else
            {
                workspace.activeObject = null;
            }
        }
        public void GroupSelect(bool sel)
        {
            if (sel)
            {
                workspace.database.SelectInterfaceObject(this);
                editorProperties.bIsGroupSelected = true;
            }
            else
            {
                workspace.database.DeselectInterfaceObject(this);
                editorProperties.bIsGroupSelected = false;
            }
        }
        public void GroupSelectPure(bool sel)
        {
            editorProperties.bIsGroupSelected = false;
        }
        public void Move(PointF pos)
        {
            editorProperties.globalPos = pos;
        }
        public PointF GetCurrentPos()
        {
            return editorProperties.globalPos;
        }
        public PointF GetGlobalPos()
        {
            return editorProperties.globalPos;
        }
        public void AddChild(InterfaceObject child)
        {
            if (child == null)
                return;

            editorProperties.children.Add(child);
        }
        public PointF GetHierarchyPos()
        {
            if(editorProperties.parentObject != null)
            {
                PointF parPos = editorProperties.parentObject.GetHierarchyPos();
                PointF locPos = editorProperties.posOnParent;
                return new PointF(parPos.X + locPos.X, parPos.Y + locPos.Y);
            }
            else
                return editorProperties.posOnParent;
        }
        public void Delete()
        {
            if(editorProperties.parentObject != null)
            {
                editorProperties.parentObject.RemoveChild(this);
                editorProperties.parentObject = null;
            }
            foreach(InterfaceObject kid in editorProperties.children)
            {
                kid.editorProperties.parentObject = null;
                workspace.database.DeleteInterfaceObject(kid);
            }
            editorProperties.children.Clear();
        }
        public void RemoveChild(InterfaceObject obj)
        {
            editorProperties.children.Remove(obj);
        }
        public void TestMode_Visible(bool vis)
        {
            editorProperties.bVisible = vis;
        }
        public SizeF GetLabelSize()
        {
            if(editorProperties.EDITOR_PROPERTIES.labelText.Length > 0)
            {
                if(editorProperties.EDITOR_PROPERTIES.labelFont != null)
                {
                    if (editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC > 0)
                    {
                        Graphics g = workspace.CreateGraphics();
                        Font font = new Font(editorProperties.EDITOR_PROPERTIES.labelFont, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, FontStyle.Regular);
                        return g.MeasureString(editorProperties.EDITOR_PROPERTIES.labelText, font);
                    }
                }
            }
            return new SizeF(0f, 0f);
        }
        public SizeF GetInputTextSize()
        {
            if (editorProperties.EDITOR_PROPERTIES.labelText.Length > 0)
            {
                if (editorProperties.EDITOR_PROPERTIES.labelFont != null)
                {
                    if (editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC > 0)
                    {
                        Graphics g = workspace.CreateGraphics();
                        Font font = new Font(editorProperties.EDITOR_PROPERTIES.labelFont, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, FontStyle.Regular);
                        SizeF returnSize = g.MeasureString(editorProperties.EDITOR_PROPERTIES.labelText, font);
                        returnSize.Width = editorProperties.baseSize.Width;
                        return returnSize;
                    }
                }
            }
            return editorProperties.baseSize;
        }
        public SizeF GetButtonSize()
        {
            Graphics g = workspace.CreateGraphics();
            //Buttons are auto scaled to label or image; it takes the biggest boundaries of both
            //we need to get these scales and choose biggest x and biggest y
            SizeF imgSize = new SizeF(0f, 0f);
            SizeF labSize = new SizeF(0f, 0f);
            if (editorProperties.EDITOR_PROPERTIES.backgroundImage != null)
                imgSize = editorProperties.EDITOR_PROPERTIES.backgroundImage.Size;
            if (editorProperties.EDITOR_PROPERTIES.labelFont != null && editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC > 0 && editorProperties.EDITOR_PROPERTIES.labelText.Length > 0)
            {
                Font font = new Font(editorProperties.EDITOR_PROPERTIES.labelFont, editorProperties.EDITOR_PROPERTIES.labelSize * CORRECT_PERC, FontStyle.Regular);
                labSize = g.MeasureString(editorProperties.EDITOR_PROPERTIES.labelText, font);
            }
            SizeF returnSize = new SizeF(imgSize.Width > labSize.Width ? imgSize.Width : labSize.Width, imgSize.Height > labSize.Height ? imgSize.Height : labSize.Height);
            return returnSize;
        }
        public void SwitchSelect(bool select)
        {
            bSwitchSelected = select;
        }
        public bool IsHidden()
        {
            if(editorProperties.parentObject != null)
            {
                return editorProperties.parentObject.IsHidden();
            }
            return editorProperties.bHide;
        }
    }
}
