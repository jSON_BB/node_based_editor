﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WorkspaceControl
{
    public static class FuryMath
    {
        public static PointF[] GetCurveAlgorithmPoints(PointF startPoint, PointF endPoint)
        {
            const int interpolation = 48;

            PointF[] points = new PointF[interpolation];
            for (int i = 0; i < interpolation; i++)
            {
                float amount = i / (float)(interpolation - 1);

                var lx = Lerp(startPoint.X, endPoint.X, amount);
                var d = Math.Min(Math.Abs(endPoint.X - startPoint.X), 100);
                var a = new PointF((float)Scale(amount, 0, 1, startPoint.X, startPoint.X + d),
                    startPoint.Y);
                var b = new PointF((float)Scale(amount, 0, 1, endPoint.X - d, endPoint.X), endPoint.Y);

                var bas = Sat(Scale(amount, 0.1, 0.9, 0, 1));
                var cos = Math.Cos(bas * Math.PI);
                if (cos < 0)
                {
                    cos = -Math.Pow(-cos, 0.2);
                }
                else
                {
                    cos = Math.Pow(cos, 0.2);
                }
                amount = (float)cos * -0.5f + 0.5f;

                var f = Lerp(a, b, amount);
                points[i] = f;
            }

            return points;
        }

        public static double Sat(double x)
        {
            if (x < 0) return 0;
            if (x > 1) return 1;
            return x;
        }


        public static double Scale(double x, double a, double b, double c, double d)
        {
            double s = (x - a) / (b - a);
            return s * (d - c) + c;
        }

        public static float Lerp(float a, float b, float amount)
        {
            return a * (1f - amount) + b * amount;
        }

        public static PointF Lerp(PointF a, PointF b, float amount)
        {
            PointF result = new PointF();

            result.X = a.X * (1f - amount) + b.X * amount;
            result.Y = a.Y * (1f - amount) + b.Y * amount;

            return result;
        }
        public static Point RotatePoint(Point ptToRotate, Point center, double angle)
        {
            Point pointToRotate = ptToRotate;
            Point centerPoint = center;
            double angleInDegrees = angle;
            double angleInRadians = angleInDegrees * (Math.PI / 180);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);

            return new Point
            {
                X =
                    (int)
                    (cosTheta * (pointToRotate.X - centerPoint.X) -
                    sinTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.X),
                Y =
                    (int)
                    (sinTheta * (pointToRotate.X - centerPoint.X) +
                    cosTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.Y)
            };
            //calculate angle between parent and child
            //PointF parentPos = parent.GetOutputPlugPoint();
            //PointF thisPos = GetInputPlugPoint();
            //PointF posDiff = new PointF(thisPos.X - parentPos.X, thisPos.Y - parentPos.Y);
            //double _c = (Math.Pow(posDiff.X, 2) + Math.Pow(posDiff.Y, 2));
            //_c = Math.Sqrt(_c);
            //double sinAlpha = posDiff.X / _c;
            //double cosAlpha = posDiff.Y / _c;
            //sinAlpha = Math.Asin(sinAlpha);
            //cosAlpha = Math.Acos(cosAlpha);
            //sinAlpha *= 57.2957795;
            //cosAlpha *= 57.2957795;
            //float angle = (float)cosAlpha;

            //angle *= -1;
            //if (sinAlpha < 0)
            //    angle *= -1;
        }

        public static string ConvertARGBToHex(Color colorToConvert)
        {
            string hex = "#" + colorToConvert.A.ToString("X2") + colorToConvert.R.ToString("X2") + colorToConvert.G.ToString("X2") + colorToConvert.B.ToString("X2");
            return hex;
        }
        public static string ConvertRGBToHex(Color colorToConvert)
        {
            string hex = "#" + colorToConvert.R.ToString("X2") + colorToConvert.G.ToString("X2") + colorToConvert.B.ToString("X2");
            return hex;
        }
    }
}
